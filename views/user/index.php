<?php

use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'options' => ['style' => 'width:75px'],
            ],
            'email',
            'phone',
            //'role',
            'adminname',
            'agencyname',
            [
                'attribute' => 'country',
                'filter' => Html::activeDropDownList(
                        $searchModel,
                        'country',
                        User::getCountriesList(),
                        ['prompt' => '', 'class' => 'form-control']
                ),
                'value' => function ($data, $key, $index, $column){
                    return User::getCountriesList()[$data->country];
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
