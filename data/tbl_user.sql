-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` enum('master','admin') COLLATE utf8_unicode_ci NOT NULL,
  `adminname` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `agencyname` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role` (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `tbl_user` (`id`, `email`, `phone`, `role`, `adminname`, `agencyname`, `country`, `password_hash`, `auth_key`) VALUES
(1,	'master',	'+380 (66) 906-00-50',	'master',	'Master',	'',	'uk',	'$2y$13$z647OvNzKjHx75Mw6TKIgOOnFsbyBLw4IXPLiFsyhSfHXzVPPVgaO',	'CI6azQaC7ycfrwexxDn48fMQh4bI-KdW'),
(2,	'drg2@dxgrrxd.ru',	'123123123123',	'admin',	'tfh',	'grd',	'uk',	'$2y$13$5YdLmZZk9qTqvwe1bC2aUuiBVOaUgAFQMAEjl.OYMWHPjQ5qoYi/W',	'3TyMkJ66VoAtXZq336d4IorNxCYXhutv'),
(3,	'bm@bm.com',	'11234561234',	'admin',	'drgdawsef',	'gr gzxdrg',	'uk',	'$2y$13$SjQVIAXDaLG/v3kfbgjKz.srFwxiKgIycm7EUNZuSlr1lSG2ia8X.',	'QTofiGFCYLbi5jp8R407EMipHNJ8SFNl'),
(4,	'bmw@bmw.com',	'1123345364',	'admin',	'dazseszgew',	'szef sef',	'uk',	'$2y$13$KVpcZ01VkYz9i73nue8SKeKOntyPAKlErJsy5FP1HqnEPNvDd6sG.',	'cgdO2srTKD_R5tpxzxcmvUG-FRRX165j'),
(5,	'bmq@bmq.com',	'1wrwsfe5461234',	'admin',	'xdhdraw',	'zsfe ',	'uk',	'$2y$13$BRUIZgAR6LJ4ij9P4vrilO1IRc6upOeCu22/Qlb0GsNMWMGAqhHhO',	'ru06xWXOthVWXXhgu06I7XyG2aLR3zeU'),
(6,	'bm1@bm1.com',	'54645561234',	'admin',	'dxgrg g drgxdrg',	'z sfszefse',	'uk',	'$2y$13$rPCft1su98oOREc3dakbbejZkUd553D4vohKZH03RkirmTADJVvV.',	'm6yerJgmZ_eRyuAJ_suvP11v1YS8Y20g');

-- 2016-12-12 21:57:12
