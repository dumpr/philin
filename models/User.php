<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%tbl_user}}".
 *
 * @property integer $id
 * @property string $email
 * @property string $phone
 * @property string $role
 * @property string $adminname
 * @property string $agencyname
 * @property string $country
 * @property string $password_hash
 * @property string $auth_key
 */
class User extends ActiveRecord implements IdentityInterface
{
    const ROLE_MASTER = 'master';
    const ROLE_ADMIN = 'admin';

    public static function getCountriesList()
    {
        return [
            'uk' => 'Ukraine',
            'ru' => 'Russia',
            'by' => 'Belarus'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'phone', 'role', 'adminname', 'agencyname', 'country'], 'required'],
            ['role', 'default', 'value' => self::ROLE_ADMIN],
            ['email', 'email'],
            [['email', 'phone', 'adminname', 'agencyname'], 'string', 'max' => 64],
            [['country'], 'string', 'max' => 8],
            [['password_hash', 'auth_key'], 'string', 'max' => 255],
        ];
    }

    /**
    id	int(11) Auto Increment
    email	varchar(64)
    phone	varchar(64) NULL
    role	enum('master','admin')
    adminname	varchar(64)
    agencyname	varchar(64)
    country	varchar(8)
    password_hash	varchar(255) NULL
    auth_key    varchar(255) NULL
     */

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'E-mail Администратора'),
            'phone' => Yii::t('app', 'Телефон Администратора'),
            'adminname' => Yii::t('app', 'ФИО администратора'),
            'agencyname' => Yii::t('app', 'Название агентства'),
            'country' => Yii::t('app', 'Страна'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'role' => Yii::t('app', 'Роль'),
        ];
    }

    public function beforeValidate()
    {
        $phoneNumeric = preg_replace('/\D/', '', $this->phone);

        if (strlen($phoneNumeric) < 11) {
            $this->addError('phone', Yii::t('app', 'Телефон должен содержать как минимум 11 цифр'));
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }
}
