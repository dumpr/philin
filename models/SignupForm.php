<?php

namespace app\models;

use Yii;
use yii\base\Model;
/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $password_repeat;
    public $phone;
    public $role;
    public $country;
    public $agencyname;
    public $adminname;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password', 'password_repeat'], 'trim'],
            [['email', 'password', 'password_repeat', 'agencyname', 'adminname', 'phone'], 'required'],
            ['email', 'email'],
            ['role', 'default', 'value' => 'admin'],
            [['phone', 'country', 'agencyname', 'adminname'], 'string', 'max' => 64],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
        ];
    }

    public function beforeValidate()
    {
        $phoneNumeric = preg_replace('/\D/', '', $this->phone);

        if (strlen($phoneNumeric) < 11) {
            $this->addError('phone', 'Phone number must contain at least 11 digits');
        }

        return parent::beforeValidate();
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->setAttributes([
            'email' => $this->email,
            'phone' => $this->phone,
            'country' => $this->country,
            'adminname' => $this->adminname,
            'agencyname' => $this->agencyname,
            'role' => 'admin',
        ]);
        $user->setPassword($this->password);
        $user->generateAuthKey();

        return $user->save() ? $user : null;
    }

    public function sendEmails($model)
    {

        $from = [Yii::$app->params['adminEmail'] => Yii::$app->params['siteName']];

        // письмо новому пользователю
        return Yii::$app->mailer->compose([
            'html' => 'welcome-html',
            'text' => 'welcome-text'
        ], [
            'model' => $model,
        ])
            ->setFrom($from)
            ->setTo($this->email)
            ->setSubject('Добро пожаловать на ' . Yii::$app->params['siteName'])
            ->send()

        &&
        // письмо мастеру о новой регистрации
        Yii::$app->mailer->compose([
            'html' => 'toMaster-html',
            'text' => 'toMaster-text'
        ], [
            'model' => $model,
        ])
            ->setFrom($from)
            ->setTo(Yii::$app->params['adminEmail'])
            ->setSubject('Новый пользователь')
            ->send();
    }
}