<?php

/** @var \app\models\User $model */

?>

На сайте <?= Yii::$app->params['siteName'] ?> была произведена регистрация нового пользователя <?= $model->adminname ?>.


Агентство: <?= $model->agencyname ?>
Контакты: <?= $model->phone ?>,
<?= $model->email ?>


Ссылка для редактирования http://<?= $_SERVER['HTTP_HOST'] ?>/user/update?id=<?= $model->id ?>
