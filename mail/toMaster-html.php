<?php

/** @var \app\models\User $model */

?>

<p>На сайте <?= Yii::$app->params['siteName'] ?> была произведена регистрация нового пользователя <?= $model->adminname ?>.</p>

<p>Агентство <?= $model->agencyname ?></p>
<p>Контакты <?= $model->phone ?>,<br /><?= $model->email ?></p>

<?php
    $url = 'http://' . $_SERVER['HTTP_HOST'] . '/user/update?id=' . $model->id;
?>
<p>Ссылка для редактирования <a href="<?= $url ?>"><?= $url ?></a></p>
